/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */

workbox.precaching.precacheAndRoute([
  {
    "url": "build/change-version.js",
    "revision": "589a9539ea0b83179ed74c6b7647aad6"
  },
  {
    "url": "build/gcp-key.json.enc",
    "revision": "d26db99024395282be8164f31200b6c4"
  },
  {
    "url": "build/generate-sri.js",
    "revision": "1db9bbe70647fca8a609691c3606f666"
  },
  {
    "url": "build/lint-vars.js",
    "revision": "972038623ddf04956f46c5cdc892ef95"
  },
  {
    "url": "build/postcss.config.js",
    "revision": "7ca37118f763411488a85a541cf8d459"
  },
  {
    "url": "build/rollup.config.js",
    "revision": "337397cf0ac9a222e60b74e06435b6c5"
  },
  {
    "url": "build/sauce_browsers.json",
    "revision": "69701653cce75721c8f37af55b956193"
  },
  {
    "url": "build/saucelabs-unit-test.js",
    "revision": "6a8677e54dd2de0c7991ca13b52e69e5"
  },
  {
    "url": "build/ship.sh",
    "revision": "f2f6511ef4ec078937b52d15860c2ade"
  },
  {
    "url": "build/upload-preview.sh",
    "revision": "3af793d7259ac8b552e4e3164aeeadbb"
  },
  {
    "url": "build/vnu-jar.js",
    "revision": "626ae749c8e231b08ff58bf6f2dfe39a"
  },
  {
    "url": "build/workbox.config.json",
    "revision": "7e7d563a25c9794b80586a6d9f9e3f12"
  },
  {
    "url": "build/workbox.js",
    "revision": "5d43d88d3c0503993001d4d072fb5a0d"
  },
  {
    "url": "css/all.css",
    "revision": "185539981482cea1299c1fdcebb598b4"
  },
  {
    "url": "css/all.min.css",
    "revision": "185539981482cea1299c1fdcebb598b4"
  },
  {
    "url": "css/bootstrap4.css",
    "revision": "65a963fe88107a220b8a22e6dfc5a52f"
  },
  {
    "url": "css/bootstrap4.min.css",
    "revision": "6bd20d5c241cf89eee5ec11b8ef15e7d"
  },
  {
    "url": "css/brands.css",
    "revision": "2f7209ad3353f92f47c4fc0cd8ad202f"
  },
  {
    "url": "css/brands.min.css",
    "revision": "2640ddac0d8f5f0a22eebe02eab7a805"
  },
  {
    "url": "css/fontawesome.css",
    "revision": "28d3b9416d99a8a4330865b80c0f5c74"
  },
  {
    "url": "css/fontawesome.min.css",
    "revision": "25d53053e9356a1f59d2f24cd5aa98e5"
  },
  {
    "url": "css/main.css",
    "revision": "a34dc077f976175cea41d7c234f4e93f"
  },
  {
    "url": "css/regular.css",
    "revision": "2d73e472c40c003e0df6c49c576a9a80"
  },
  {
    "url": "css/regular.min.css",
    "revision": "714e2d4110323b1389754012d32621c5"
  },
  {
    "url": "css/solid.css",
    "revision": "cd360deb7fadc43765275bafa3abd509"
  },
  {
    "url": "css/solid.min.css",
    "revision": "e3b60aca41acb73c58614d9a131769fb"
  },
  {
    "url": "css/svg-with-js.css",
    "revision": "3fb2b2e201792d5ce3779488aa41cd01"
  },
  {
    "url": "css/svg-with-js.min.css",
    "revision": "5dce839e93cfd1edba341e20c9322022"
  },
  {
    "url": "css/v4-shims.css",
    "revision": "ff504f33c73b8a19b4c879faea96ab44"
  },
  {
    "url": "css/v4-shims.min.css",
    "revision": "7638dafdfaef6e4ca427ac08225aa564"
  },
  {
    "url": "images/57display.bmp",
    "revision": "c7f4f11320e1bd43ad8838652d6b0adf"
  },
  {
    "url": "images/57fixieren.bmp",
    "revision": "f7872157b1b345fccd3f9fc7e50c80ca"
  },
  {
    "url": "images/anheften.bmp",
    "revision": "050c3e76fc46448a87f05d43b2fc842a"
  },
  {
    "url": "images/anschleifen.png",
    "revision": "0a5e8bcc60ea029bf655e275e1f96835"
  },
  {
    "url": "images/anschliessen.png",
    "revision": "baee56c71b0cdb56dd6650c5fcf4d847"
  },
  {
    "url": "images/arbeiten_welder.png",
    "revision": "595e77c24629c94500a880888908038c"
  },
  {
    "url": "images/argon.png",
    "revision": "5ce13445912ab71b53aa577e95a60aa4"
  },
  {
    "url": "images/background.jpg",
    "revision": "62cef510c67363597450c7148d5807ee"
  },
  {
    "url": "images/Bitmap113.jpg",
    "revision": "d3c95c697102ef664cc11d7a2c1d840d"
  },
  {
    "url": "images/Bitmap113.png",
    "revision": "0bd76d140ae51050729615bda261881b"
  },
  {
    "url": "images/draht.jpg",
    "revision": "58e1092e84b9d376a8a6d3383dd42698"
  },
  {
    "url": "images/DSC_2713.JPG",
    "revision": "c8802a41fed1e8989181a3aec670b0f1"
  },
  {
    "url": "images/dummy_display_IT.bmp",
    "revision": "72f467c6a5fbf2c924ed69695ff079d4"
  },
  {
    "url": "images/dummy_display.bmp",
    "revision": "f97a9ea05b2ea3ee794d6b9a798414b3"
  },
  {
    "url": "images/dummy_display.jpg",
    "revision": "169d433913c70f76e376d1c7e6136ecb"
  },
  {
    "url": "images/dummy_display.png",
    "revision": "1d820f6efa872dcc046b73d3a03bf8bd"
  },
  {
    "url": "images/ecpert.PNG",
    "revision": "62f95f6e7010065c4a66dc46118b8036"
  },
  {
    "url": "images/einstellungen_DE_plus.png",
    "revision": "e014d2e469285bf8270fa83561338fa1"
  },
  {
    "url": "images/einstellungen_de.bmp",
    "revision": "b17b0f7a0d15d393b7f609a26a82e1ce"
  },
  {
    "url": "images/einstellungen_en.bmp",
    "revision": "87fa0e78722baad6e100efec0f52dd84"
  },
  {
    "url": "images/einstellungen_EN.png",
    "revision": "fee0038d6bd232a276fad2d78385af4c"
  },
  {
    "url": "images/einstellungen_fr.bmp",
    "revision": "830e1016022bbf2d6d5cdf6b38896ef6"
  },
  {
    "url": "images/einstellungen_FR.png",
    "revision": "4c7e3af3022605d2fab2492cc7002e84"
  },
  {
    "url": "images/einstellungen_it.bmp",
    "revision": "f34eab853b2f05fb26e57dc89009ca8a"
  },
  {
    "url": "images/einstellungen_plus_FR.bmp",
    "revision": "b008b7db6149abfdd7ac13388e0d27ce"
  },
  {
    "url": "images/einstellungen_plus_IT.bmp",
    "revision": "f34eab853b2f05fb26e57dc89009ca8a"
  },
  {
    "url": "images/einstellungen_plus.bmp",
    "revision": "b17b0f7a0d15d393b7f609a26a82e1ce"
  },
  {
    "url": "images/einstellungen.png",
    "revision": "6e0da46189174426e1f0d46a1ece4ce1"
  },
  {
    "url": "images/elektrode.png",
    "revision": "0751743fdc42f51ba87bf8ab3e57acc2"
  },
  {
    "url": "images/ElektrodeSchleifen.png",
    "revision": "cf3604555d2445b1610a1d2bb1029eba"
  },
  {
    "url": "images/fixierhandstueck_2.jpg",
    "revision": "78491d6ecca8b53bbb10ac31b282978e"
  },
  {
    "url": "images/Fixierschweissen.jpg",
    "revision": "0d2981a11a77fb7d4ff83d8933ee8f4b"
  },
  {
    "url": "images/flags/de.png",
    "revision": "c9610f0d2105dc26c2fc25fa4c41d190"
  },
  {
    "url": "images/flags/en.png",
    "revision": "a8530a96e01c7d218faf8ac83f999662"
  },
  {
    "url": "images/flags/es.png",
    "revision": "ee2ec7b1a2b3dbb0bed31634b09eb836"
  },
  {
    "url": "images/flags/fr.png",
    "revision": "35b23cb8a9ace025ac01e41ae8006d62"
  },
  {
    "url": "images/flags/it.png",
    "revision": "b5a111f02c80d1380474c9a00b3c6ba2"
  },
  {
    "url": "images/flags/po.png",
    "revision": "65bed34fc4bd9cdbdfc780b12540b66d"
  },
  {
    "url": "images/Fuss_on.bmp",
    "revision": "a7fd70cc3df9a45a2f355e356ce8d249"
  },
  {
    "url": "images/glaetten_on.bmp",
    "revision": "eb895fbe9d173a7fe7d03398bdd6e24e"
  },
  {
    "url": "images/Glinfo.bmp",
    "revision": "c690bd009b56428ed870626212db7c0e"
  },
  {
    "url": "images/handstueck3.png",
    "revision": "3d3c3b1f494a14f59d0ce3035a6ef765"
  },
  {
    "url": "images/Herzschrittmacher.png",
    "revision": "be95a7ee4da3ee2231672fed53a8af9b"
  },
  {
    "url": "images/icons/icon-128x128.png",
    "revision": "55774536eb294535bb456f0ceac865d9"
  },
  {
    "url": "images/icons/icon-144x144.png",
    "revision": "bb1af34c334fb6b61a94c056692e44e3"
  },
  {
    "url": "images/icons/icon-152x152.png",
    "revision": "002df3a1764dbe418ba68e1fcb64cb04"
  },
  {
    "url": "images/icons/icon-192x192.png",
    "revision": "d51f9aee839c80eac9fdad613dc04577"
  },
  {
    "url": "images/icons/icon-384x384.png",
    "revision": "74fcecad4f2bc6ce216f804d2480ec64"
  },
  {
    "url": "images/icons/icon-512x512.png",
    "revision": "10965836a85699ddce30c7ca747d8e2d"
  },
  {
    "url": "images/icons/icon-72x72.png",
    "revision": "710805f7ab87af89135bf889c1ca58e0"
  },
  {
    "url": "images/icons/icon-96x96.png",
    "revision": "db9b8b52d4eeb7ffacf39f8b667e237a"
  },
  {
    "url": "images/mikro_6cm.jpg",
    "revision": "acd90553500b81a85476a035f72f8a6c"
  },
  {
    "url": "images/mikro.png",
    "revision": "e3318e9fe0aceaf39ff6b5ac8740a72e"
  },
  {
    "url": "images/mikroskop.bmp",
    "revision": "f880e89aad8d041353951a61abe70c33"
  },
  {
    "url": "images/mikroskop.jpg",
    "revision": "71fa0941d57fb6376cd8051bec5acc26"
  },
  {
    "url": "images/PiggyWelder 3 Rückplatten beschriften Rev.1_1.bmp",
    "revision": "c09e5cbb3b39ab323d45beff5aed6eca"
  },
  {
    "url": "images/PiggyWelder-3-Rückplatten-beschriften-Rev.jpg",
    "revision": "44a62455cda4f95955ade461b57e1a4c"
  },
  {
    "url": "images/piggyWelder2Startbild.png",
    "revision": "229ac1b82695b1ccdbdbd01dee5be9ed"
  },
  {
    "url": "images/s23_de.bmp",
    "revision": "3e22d0a59753f6d7af5f40deef384524"
  },
  {
    "url": "images/s23_en.bmp",
    "revision": "648a9c94a580fe49b7798f4c403ee0ab"
  },
  {
    "url": "images/s23_es.bmp",
    "revision": "1d8d7771f303202337229bb5f9b6f50a"
  },
  {
    "url": "images/s23_fr.bmp",
    "revision": "c1096af23bf86b338e21abaf29addb2c"
  },
  {
    "url": "images/s23_it.bmp",
    "revision": "19a195d723a38d7daa7aac56bc29e6d6"
  },
  {
    "url": "images/s23_po.bmp",
    "revision": "0cea5c904b154e416da1445ddb4d77dd"
  },
  {
    "url": "images/s28_de.bmp",
    "revision": "a447543358a97884ac4f412fedb19ea9"
  },
  {
    "url": "images/s28_en.bmp",
    "revision": "8ccc047f067e4cbbd341bfb77476f042"
  },
  {
    "url": "images/s28_es.bmp",
    "revision": "af2dfce03739077fed00493e42113b19"
  },
  {
    "url": "images/s28_fr.bmp",
    "revision": "8ed187d685bc90063b95ee0338e03d9d"
  },
  {
    "url": "images/s28_it.bmp",
    "revision": "bcadcd711e2d4eff416dd7f8d5d653a1"
  },
  {
    "url": "images/s28_po.bmp",
    "revision": "3d50d963a414c6d60d683394b4559ed3"
  },
  {
    "url": "images/s30_de.bmp",
    "revision": "1fdcb08ddf5b36f56e99ab5261ad2a53"
  },
  {
    "url": "images/s30_en.bmp",
    "revision": "c3074a6202845e18272ed6d7f99368cf"
  },
  {
    "url": "images/s30_es.bmp",
    "revision": "e73a2c26c4cfcac8d78eb4def23f2e8f"
  },
  {
    "url": "images/s30_fr.bmp",
    "revision": "7b7a8013a60dce7fa1f8b7d91940e3b7"
  },
  {
    "url": "images/s30_it.bmp",
    "revision": "add0651ac418937472626f5559f25a88"
  },
  {
    "url": "images/s30_po.bmp",
    "revision": "4e58517a099c3637599123238f3a76b6"
  },
  {
    "url": "images/s41_de.bmp",
    "revision": "d3949208f1782d65f258cbd1229c1e6c"
  },
  {
    "url": "images/s41_en.bmp",
    "revision": "97991c9b0c0e9c2cf6bafe0ad9e42dd4"
  },
  {
    "url": "images/s41_es.bmp",
    "revision": "9add1cb2b629e6202f5402bded4f974a"
  },
  {
    "url": "images/s41_fr.bmp",
    "revision": "feb526fcded257e176e9f19505d4feed"
  },
  {
    "url": "images/s41_it.bmp",
    "revision": "bcaf662494c4ac0fc59c649d35c55e16"
  },
  {
    "url": "images/s41_po.bmp",
    "revision": "4e4e14fa1113122517d17d53ec31501e"
  },
  {
    "url": "images/SaveUnterweisung.png",
    "revision": "8179b4142d516e0c53a47e59b90a3f4e"
  },
  {
    "url": "images/schwein_gr.png",
    "revision": "56b91db4210e2a1ccae413041619892f"
  },
  {
    "url": "images/schwein_transparent_5cm.png",
    "revision": "1a445fe55d5a8baad5e377c81beb647f"
  },
  {
    "url": "images/seminarraum.png",
    "revision": "4a665f4e4721ff5983bfea07db34690b"
  },
  {
    "url": "images/speed_de.bmp",
    "revision": "c690bd009b56428ed870626212db7c0e"
  },
  {
    "url": "images/speed_en.bmp",
    "revision": "a67e1494d27a7ef42fe29e761b33ec22"
  },
  {
    "url": "images/speed_it.bmp",
    "revision": "e119023c12f256824667401480849dfe"
  },
  {
    "url": "images/warnung.png",
    "revision": "3d34715bb248ccffb5149a7ae5580bc8"
  },
  {
    "url": "images/welder3_display_plus.bmp",
    "revision": "aa29ede054383b41b321c369b5b6ccfc"
  },
  {
    "url": "images/welder3_fixieren.bmp",
    "revision": "117092f6117b6c94335f0553807eaa9d"
  },
  {
    "url": "images/welder3_fixieren.tif",
    "revision": "e01b23ea99d2c5d662cef1b8d71875ab"
  },
  {
    "url": "images/welder3_mikro.bmp",
    "revision": "26a9151bf47473288914bdc92074b918"
  },
  {
    "url": "images/welder3_schweissen.bmp",
    "revision": "4a2cff20dac4c887851630871e23b213"
  },
  {
    "url": "images/welder3_steuereinheit_PLUS.bmp",
    "revision": "b8453eb24e8d06853da8ff2ee242c8a8"
  },
  {
    "url": "images/welder3_steuereinheit_PLUS.jpg",
    "revision": "34e9f0b5b183726abb45d5d26bc2e2e6"
  },
  {
    "url": "images/welder3_totale_PLUS.bmp",
    "revision": "7e25c966b95a618132efda67192c4e96"
  },
  {
    "url": "images/welder3_warnung_plus.bmp",
    "revision": "888757b4635ca9374f1cd54a9ff335be"
  },
  {
    "url": "images/widerstand_de.png",
    "revision": "cf1f9ffc992fdb93fdae3b7dd0d8decb"
  },
  {
    "url": "images/widerstand_en.png",
    "revision": "96dc20a5ad448727df9e502490c54be5"
  },
  {
    "url": "images/widerstand_eng.png",
    "revision": "96dc20a5ad448727df9e502490c54be5"
  },
  {
    "url": "images/widerstand_fr.png",
    "revision": "dd35b98462b49f000db043833dd4a13e"
  },
  {
    "url": "images/widerstand_IT.png",
    "revision": "111ee31809ecfef17c38b7b6b0a1995b"
  },
  {
    "url": "images/widerstand.png",
    "revision": "cf1f9ffc992fdb93fdae3b7dd0d8decb"
  },
  {
    "url": "images/winkel.png",
    "revision": "0099c17215d4c0ae52ac78c4ca8cb137"
  },
  {
    "url": "index.html",
    "revision": "82aaf3d5983944ff71e7d4007071567b"
  },
  {
    "url": "js/all.js",
    "revision": "5e56169cc4251ed2c4911d7e52b40155"
  },
  {
    "url": "js/all.min.js",
    "revision": "1fd486434f92cbcf81d7346bf5457165"
  },
  {
    "url": "js/app.js",
    "revision": "1925532455d9a611670e9a36a77bdecf"
  },
  {
    "url": "js/bootstrap.bundle.js",
    "revision": "bd1604da466f7535f07899082f104eec"
  },
  {
    "url": "js/bootstrap.bundle.min.js",
    "revision": "b41fe9374205bd087a4d4f0ab5a195be"
  },
  {
    "url": "js/bootstrap.js",
    "revision": "5e7d168ed3203dab385e83f97f98f725"
  },
  {
    "url": "js/bootstrap.min.js",
    "revision": "0a958254db529f99f475080fe2a6dcdb"
  },
  {
    "url": "js/brands.js",
    "revision": "1cb6d7e23cd0556dda91297495f972dc"
  },
  {
    "url": "js/brands.min.js",
    "revision": "652baab2e187c8e988b6910ac2d18264"
  },
  {
    "url": "js/fontawesome.js",
    "revision": "83c9fc9c03610dc114afe74ab2e00379"
  },
  {
    "url": "js/fontawesome.min.js",
    "revision": "07470cbc5376411eefeb156977b187b8"
  },
  {
    "url": "js/gridtile_3x3.png",
    "revision": "32962e55384eb5f08af6d123119f98cf"
  },
  {
    "url": "js/jquery-3.4.1.min.js",
    "revision": "2f772fed444d5489079f275bd01e26cc"
  },
  {
    "url": "js/regular.js",
    "revision": "57ba8f19393229e5484e87662d5c8393"
  },
  {
    "url": "js/regular.min.js",
    "revision": "86b7c59bc13c54ef65f4da8eb6f7a937"
  },
  {
    "url": "js/solid.js",
    "revision": "f44e0f1a0fca2e128622b98655ff29de"
  },
  {
    "url": "js/solid.min.js",
    "revision": "edb2437711c2c2dde50485a0e227dabe"
  },
  {
    "url": "js/swipe.js",
    "revision": "91dbfe01f6398805670dcb4481e8e14d"
  },
  {
    "url": "js/TouchSwipe/bower.json",
    "revision": "b0bc91a95b111d32f9eff5130a13fb5c"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.html",
    "revision": "da110052b62497d5f92edc91dc5f4850"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.swipe.defaults.html",
    "revision": "6713e10f805e91f88b065cc13358ea19"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.swipe.directions.html",
    "revision": "2a41c5928262eec9909a8994226237ec"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.swipe.fingers.html",
    "revision": "c11a3f083ca3df95408d702754b6f501"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.swipe.html",
    "revision": "2d6cf51935137d7b20b58212a1294a4d"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.swipe.pageScroll.html",
    "revision": "30dbb2709755f6dd0a82314ad7399dfc"
  },
  {
    "url": "js/TouchSwipe/docs/$.fn.swipe.phases.html",
    "revision": "68b1b65f77d6d54f0ce1694157af1324"
  },
  {
    "url": "js/TouchSwipe/docs/$.html",
    "revision": "f2ce7d7b3a03d6ff2c4daed3ebc111c5"
  },
  {
    "url": "js/TouchSwipe/docs/css/main.css",
    "revision": "0f085504eddeee46ece1af29bc8fc726"
  },
  {
    "url": "js/TouchSwipe/docs/index.html",
    "revision": "0e97ec34e1ec5652add09b43c0de9ecc"
  },
  {
    "url": "js/TouchSwipe/docs/jquery.touchSwipe.js.html",
    "revision": "7ef1d3b0790ddc05dfda12586ac0773b"
  },
  {
    "url": "js/TouchSwipe/docs/js/jquery.ui.ipad.js",
    "revision": "14a4468a5a8b55c3fe2b4f1d9f902b97"
  },
  {
    "url": "js/TouchSwipe/docs/js/main.js",
    "revision": "7c80ad7cd2c51a0a563120b6ee2236e5"
  },
  {
    "url": "js/TouchSwipe/docs/scripts/linenumber.js",
    "revision": "634c181af320c3927c7c64e53f14a684"
  },
  {
    "url": "js/TouchSwipe/docs/scripts/prettify/Apache-License-2.0.txt",
    "revision": "d273d63619c9aeaf15cdaf76422c4f87"
  },
  {
    "url": "js/TouchSwipe/docs/scripts/prettify/lang-css.js",
    "revision": "99101deec02d75e5d0ad1d7cd1b2c35c"
  },
  {
    "url": "js/TouchSwipe/docs/scripts/prettify/prettify.js",
    "revision": "b56ed594da5b2708e3b71df6185a12b4"
  },
  {
    "url": "js/TouchSwipe/docs/styles/jsdoc.css",
    "revision": "8aef56496179d44c5643b1776f30fe86"
  },
  {
    "url": "js/TouchSwipe/docs/styles/prettify.css",
    "revision": "c55636e5b9da6fc4fe427cbeed308963"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Any_finger_swipe.html",
    "revision": "cadd5dc2573386e41f9ba1324f9430a5"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Basic_swipe.html",
    "revision": "cf8b6edab9eb6c8b0d288871c113b451"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Enable_and_destroy.html",
    "revision": "4d8a331f8411aeee663d350c9e6423b0"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Excluded_children.html",
    "revision": "fbfc63c83ca36e6420e9ad3008abf04a"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Finger_swipe.html",
    "revision": "b5dc9cb6a1e929258000d2da9f9c97e8"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Handlers_and_events.html",
    "revision": "f6b4488655b07c896377f3ec5d7d9257"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Hold.html",
    "revision": "a002bc6c9443c76f9f5bd2ff36f8b08a"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Image_gallery_example.html",
    "revision": "9f10f666793fd131b78aeec5daed131c"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-index_.html",
    "revision": "fe054039b15e455b3f0ee4cfe6ebd791"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Options.html",
    "revision": "3eabbdfc96068aecc8060e718e4cffa4"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Page_scrolling.html",
    "revision": "675255d1ef2f73f76c81a63f71bd00a5"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Page_zoom.html",
    "revision": "cb1b3944cb5b97c52b58b7d6ba095979"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Pinch_and_Swipe.html",
    "revision": "acdc947ece6e36e4f91d049a9d6032c9"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Pinch_status.html",
    "revision": "f1d134ee6d2edb4455bf31d9aebec09a"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Pinch.html",
    "revision": "3d4836e6384cd7a0e23cc06e91c548a6"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Single_swipe.html",
    "revision": "2bedfc79128b13a9e00693848a51c0ae"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Stop_propegation.html",
    "revision": "7994ec19d81d5f6e02d81dba7f0820bf"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Swipe_status.html",
    "revision": "384e216df543679390a90a2129de1434"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Tap_vs_swipe.html",
    "revision": "9cb836cf4911c9edb5b4824e94a2db10"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Thresholds.html",
    "revision": "f5a0869156d108962db7e7a9244322ad"
  },
  {
    "url": "js/TouchSwipe/docs/tutorial-Trigger_handlers.html",
    "revision": "c6940a3075a515b339a1c533733aa5a0"
  },
  {
    "url": "js/TouchSwipe/jquery.touchSwipe.js",
    "revision": "6560e52b9e9756399a4c21d92e18ce8c"
  },
  {
    "url": "js/TouchSwipe/jquery.touchSwipe.min.js",
    "revision": "4240585c4b60dec10c52b882c20c469e"
  },
  {
    "url": "js/TouchSwipe/package.json",
    "revision": "7476b8e7a05312d3e2921ff7f488e15c"
  },
  {
    "url": "js/TouchSwipe/README.md",
    "revision": "f93b09f1892d1b2b59908c32409ab95d"
  },
  {
    "url": "js/v4-shims.js",
    "revision": "f5480d4f8e462b9906fabeb01c983f18"
  },
  {
    "url": "js/v4-shims.min.js",
    "revision": "469a28fbf5c2f3961a9d84ce5a1116ca"
  },
  {
    "url": "js/workbox-sw.js",
    "revision": "89176cf5ffb62c23e38b6ad354d5521f"
  },
  {
    "url": "manifest.json",
    "revision": "de6843c9fbd3d4f6924296007e0dbded"
  },
  {
    "url": "package-lock.json",
    "revision": "5d0496ef3a4b33cf0b1f31496d3abbd2"
  },
  {
    "url": "package.js",
    "revision": "429bd14a6e9a59141f9273f29d45f878"
  },
  {
    "url": "package.json",
    "revision": "22682968ab27a32ae63abf511ea94968"
  },
  {
    "url": "robots.txt",
    "revision": "bbbcde0b15cabd06aace1df82d335978"
  },
  {
    "url": "seiten/10_anschluesse.html",
    "revision": "62eaa345d1eb3095ea97fa09d9bb818a"
  },
  {
    "url": "seiten/11_handstueck.html",
    "revision": "eece2200e4f5dec90a3759182a283b90"
  },
  {
    "url": "seiten/12_handstueck.html",
    "revision": "7d15fb95bd118607b6be1e1233cf16c7"
  },
  {
    "url": "seiten/13_handstueck.html",
    "revision": "8652ffaaf095478b948dd4927595f791"
  },
  {
    "url": "seiten/14_handstueck.html",
    "revision": "dc9552fb9bda16b5bf46b0216fac13e5"
  },
  {
    "url": "seiten/15_handstueck.html",
    "revision": "8317cec32a3fa31ea6b87a9ae6c5a4c7"
  },
  {
    "url": "seiten/16_handstueck.html",
    "revision": "9fbef30889f6295ea0659c3d70abe76b"
  },
  {
    "url": "seiten/17_handstueck.html",
    "revision": "5fee18fa6e784eb48534aef91d372608"
  },
  {
    "url": "seiten/18_handstueck.html",
    "revision": "bfc31766864f1a943537d5edee27b030"
  },
  {
    "url": "seiten/19_mikroskop.html",
    "revision": "aa8e1e17ba421e96acdbf4ff837ca87d"
  },
  {
    "url": "seiten/2_piggyWelder.html",
    "revision": "1f2d84bbe5fce9c36b6999f7f5109bce"
  },
  {
    "url": "seiten/20_mikroskop.html",
    "revision": "47af7c02b7f6a3cf0327e571486763fd"
  },
  {
    "url": "seiten/21_mikroskop.html",
    "revision": "c7e7373f83f322ae6ccd157179b7059c"
  },
  {
    "url": "seiten/22_mikroskop.html",
    "revision": "c0b2ca51ec0a318b4c860e3f641f3e3b"
  },
  {
    "url": "seiten/23_mikroskop.html",
    "revision": "9fe67e206fa3f41a596605cb175d4813"
  },
  {
    "url": "seiten/24_display.html",
    "revision": "0a13f718ed8ebe9200dcf5a35d4bf13a"
  },
  {
    "url": "seiten/25_display.html",
    "revision": "034ba4e8261046d63b3accb74a25c633"
  },
  {
    "url": "seiten/26_display.html",
    "revision": "e999b0cb1947c6309b64e190571c5f3e"
  },
  {
    "url": "seiten/27_display.html",
    "revision": "2ea16c6038e221d5c356cb23a9623d48"
  },
  {
    "url": "seiten/28_display.html",
    "revision": "d38dd6dc3f9cb5f93a6f1828d2cc364e"
  },
  {
    "url": "seiten/29_display.html",
    "revision": "d013d8265f3729998907bfe1e47fca21"
  },
  {
    "url": "seiten/3_sicherheitsvorschriften.html",
    "revision": "2098550d5a3804fc4a47b3edeca6e64a"
  },
  {
    "url": "seiten/30_display.html",
    "revision": "2cad91e32cca04ac75176c848ae4961b"
  },
  {
    "url": "seiten/31_display.html",
    "revision": "c4e41b8bc0c6b7cf4fe90be5fd99e539"
  },
  {
    "url": "seiten/32_display.html",
    "revision": "b807daadbb7d5bbff228e8c0679a0f0c"
  },
  {
    "url": "seiten/33_display.html",
    "revision": "d1273cd87690191c838f323e431958f4"
  },
  {
    "url": "seiten/34_prinzip.html",
    "revision": "ee2ad88da2716b1ea10ecd65eb782a27"
  },
  {
    "url": "seiten/35_prinzip.html",
    "revision": "2a2cdde7b03f00cdc06c79b24f313f4c"
  },
  {
    "url": "seiten/36_prinzip.html",
    "revision": "adf6e6fd390de9703e7f47ce6fe8554d"
  },
  {
    "url": "seiten/37_prinzip.html",
    "revision": "88d90dfae63b92b593811d9794500bb1"
  },
  {
    "url": "seiten/38_prinzip.html",
    "revision": "4160a5f1bc613fb9641ec4905634fe31"
  },
  {
    "url": "seiten/39_prinzip.html",
    "revision": "0e1cf7310e924f5fbfd57beda34f64d8"
  },
  {
    "url": "seiten/4_sicherheitsvorschriften.html",
    "revision": "66de48ae5ca35ddd91119f00f6ebf422"
  },
  {
    "url": "seiten/40_prinzip.html",
    "revision": "9581fd648b844ca9bc141d421215aea5"
  },
  {
    "url": "seiten/41_prinzip.html",
    "revision": "6cd03b5c9361937f1cf005decd3b0cff"
  },
  {
    "url": "seiten/42_prinzip.html",
    "revision": "f66dd2694145647810ab64867db66fde"
  },
  {
    "url": "seiten/43_prinzip.html",
    "revision": "c3ddb7c8bf499ccb89cb1d464bead470"
  },
  {
    "url": "seiten/44_prinzip.html",
    "revision": "31cfe1cbcc40d0fef4f136274a8a23c4"
  },
  {
    "url": "seiten/45_prinzip.html",
    "revision": "5d218209051c303c06d41c4b9445d657"
  },
  {
    "url": "seiten/46_prinzip.html",
    "revision": "0bd84cfc30cd63402cdfc3ec64a7685f"
  },
  {
    "url": "seiten/47_prinzip.html",
    "revision": "6024d232db245dc62a78dc0b2a171e0f"
  },
  {
    "url": "seiten/48_prinzip.html",
    "revision": "e02f4bb8878f76803fd450e4931fa8f5"
  },
  {
    "url": "seiten/49_prinzip.html",
    "revision": "1fbda93ae53f70bcd3f31ccd31b4bc2c"
  },
  {
    "url": "seiten/5_sicherheitsvorschriften.html",
    "revision": "ca756a9a96d611c342dcedb501497a7a"
  },
  {
    "url": "seiten/50_zeitdruck.html",
    "revision": "d281006db2fab22abfb2aef0a7d4e88f"
  },
  {
    "url": "seiten/51_draehte.html",
    "revision": "1cd1a5826de9ff50cf6a515c9e80365d"
  },
  {
    "url": "seiten/52_draehte.html",
    "revision": "371ba6881a06d4732601c6e09c19fac5"
  },
  {
    "url": "seiten/53_draehte.html",
    "revision": "8d731e017a316fc9f18f1fafe976eb4f"
  },
  {
    "url": "seiten/54_draehte.html",
    "revision": "98e900e4bf8035082223d637c70e2af1"
  },
  {
    "url": "seiten/55_draehte.html",
    "revision": "f218cbc2925defe9ec4aa2248ab97890"
  },
  {
    "url": "seiten/56_draehte.html",
    "revision": "9f270e4df08c000fd6bbc0fd76db293e"
  },
  {
    "url": "seiten/57_draehte.html",
    "revision": "315fb80cf44bab0d01f449d3da83286e"
  },
  {
    "url": "seiten/58_fertig.html",
    "revision": "d2454a2f796ff9aae7ec5d5174e8949b"
  },
  {
    "url": "seiten/6_sicherheitsvorschriften.html",
    "revision": "7cd5d32bd340f63375fa92680450b340"
  },
  {
    "url": "seiten/7_sicherheitsvorschriften.html",
    "revision": "0fbfb150c8cf7c687d5dd7af0fc45f5f"
  },
  {
    "url": "seiten/8_anschluesse.html",
    "revision": "9a4c097b7986a5d683757b4d93a2f290"
  },
  {
    "url": "seiten/9_anschluesse.html",
    "revision": "b5be45b1de6d68a71c379fe3063a381d"
  },
  {
    "url": "sw.js",
    "revision": "777aa5d960e6641486a351cd7d9e6f14"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation1_1.bmp",
    "revision": "cd8e58f86acd1872b03c156d613fcf89"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation1_1.bmp.xmp",
    "revision": "c3297299bc7327cac6591e8011f035b1"
  },
  {
    "url": "video/animation/Sequenz 400x400 Animation1.mp4",
    "revision": "bc6f59dae2c1042e41a9531940e047a4"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation2_1.mp4",
    "revision": "aa8490ab735901a2963eb3a2e5df5712"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation2_2.mp4",
    "revision": "5b8d7a3c084e691847db9bdd362d332c"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation2_3.bmp",
    "revision": "94cc7dbdb95861252c1bc8b87eb42eda"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation2_3.bmp.xmp",
    "revision": "a920892c2e7c535132651a51df77e767"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation3_1.bmp",
    "revision": "dfe5d9ee21e54dbcf8e667732e6c6cf0"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation3_1.bmp.xmp",
    "revision": "d5b97d9421d69c8da56c1fecc6dda943"
  },
  {
    "url": "video/animation/Sequenz 400x400 ANimation3_2.mp4",
    "revision": "a0846c940f0895123aacb2f4ea80cb1d"
  },
  {
    "url": "video/animation/Sequenz 400x500 ANimation3_3.mp4",
    "revision": "ab87132eeb32f0fcd795c38d340dcc7c"
  },
  {
    "url": "video/animation/Sequenz ANimation10_1.mp4",
    "revision": "c0bd6a7820fe893c43d80c84e632956e"
  },
  {
    "url": "video/animation/Sequenz ANimation10_2.mp4",
    "revision": "f1a3f60031ef2697ef19ce92e9a45b68"
  },
  {
    "url": "video/animation/Sequenz ANimation11_1.mp4",
    "revision": "30e5a37c978b97a901ac30e54093508b"
  },
  {
    "url": "video/animation/Sequenz ANimation11_2.mp4",
    "revision": "407663b5d0a7f1ae4bbc6398d5ef4dc1"
  },
  {
    "url": "video/animation/Sequenz ANimation11_3.mp4",
    "revision": "98625ea4fdc3a51a055e691589a97583"
  },
  {
    "url": "video/animation/Sequenz ANimation12.mp4",
    "revision": "214eefb5bff3a4d8555b9d9eadef4a9a"
  },
  {
    "url": "video/animation/Sequenz ANimation13_1_1.mp4",
    "revision": "87e887eb2cea8aa6c40b6fd4c4b4f59a"
  },
  {
    "url": "video/animation/Sequenz ANimation13_1.mp4",
    "revision": "7db509da28f8053ea2ececb0f71bf356"
  },
  {
    "url": "video/animation/Sequenz ANimation13_2.mp4",
    "revision": "45f1e541b93075de1c27b992af7dfead"
  },
  {
    "url": "video/animation/Sequenz ANimation13_3.bmp",
    "revision": "607adc30e4115ad4f9de50f53a7decb1"
  },
  {
    "url": "video/animation/Sequenz ANimation14_1.mp4",
    "revision": "0709d048265753f53d65a9c707499d53"
  },
  {
    "url": "video/animation/Sequenz ANimation14_2.mp4",
    "revision": "45626c6a57bf34eead06b3d59a38230c"
  },
  {
    "url": "video/animation/Sequenz ANimation4_1.mp4",
    "revision": "422c0af5e7c3f50c4ee9ea56bb987002"
  },
  {
    "url": "video/animation/Sequenz ANimation4_2.mp4",
    "revision": "2d31982f7e88915ed147442171972878"
  },
  {
    "url": "video/animation/Sequenz ANimation5_1.bmp",
    "revision": "d6b9afe8aef5ffb25437d1103079824b"
  },
  {
    "url": "video/animation/Sequenz ANimation5_1.bmp.xmp",
    "revision": "c65839c19f4298d2f252cdaff492db8e"
  },
  {
    "url": "video/animation/Sequenz ANimation5_1.jpg",
    "revision": "e82898930e3add2b52649653e48ae6aa"
  },
  {
    "url": "video/animation/Sequenz ANimation6_2.bmp",
    "revision": "604c214a9e9eb54fa300bbcf41e2b848"
  },
  {
    "url": "video/animation/Sequenz Animation6-1.mp4",
    "revision": "239bb45dcfe32cdadc8d8c07aaeadc85"
  },
  {
    "url": "video/animation/Sequenz ANimation7_1.bmp",
    "revision": "1f3b99f552c771e66c6b46bffcefe241"
  },
  {
    "url": "video/animation/Sequenz ANimation7_2.bmp",
    "revision": "2ce96917503bd33d961ab3202763a8eb"
  },
  {
    "url": "video/animation/Sequenz ANimation7_3.bmp",
    "revision": "858788e2fecdd1021731618a87b310f1"
  },
  {
    "url": "video/animation/Sequenz ANimation7_4.mp4",
    "revision": "7e43bd2fc42c688843b9dc53f7650b76"
  },
  {
    "url": "video/animation/Sequenz ANimation8_1.mp4",
    "revision": "207889f9f21bcc9e3766ebdd0b75987f"
  },
  {
    "url": "video/animation/Sequenz ANimation8_2.bmp",
    "revision": "16bb14c7cbb28a0ad67d8390aa0fc2af"
  },
  {
    "url": "video/animation/Sequenz ANimation8_3.bmp",
    "revision": "17cd3a408db730baba82daa7fd6e48e8"
  },
  {
    "url": "video/animation/Sequenz ANimation9.bmp",
    "revision": "69b1c395813b46ea57d54664c55997ed"
  },
  {
    "url": "video/animation/Thumbs.db",
    "revision": "ebb77bb425bc66d6c1295c5b8fd37058"
  },
  {
    "url": "video/mp4_Videos/fixieren_DE.m4v",
    "revision": "2a5c5025373d1b2478bc33d97fb8799b"
  },
  {
    "url": "video/mp4_Videos/fixieren_EN.m4v",
    "revision": "e7dbbe640558638160286ab474f023fe"
  },
  {
    "url": "video/mp4_Videos/fixieren_FR.m4v",
    "revision": "e48ce900d031b722418edbc7857b1d9f"
  },
  {
    "url": "video/mp4_Videos/fixieren_IT.m4v",
    "revision": "d14724efe3b40617a71d4184d88e4188"
  },
  {
    "url": "video/mp4_Videos/fixieren_PO.m4v",
    "revision": "0f3520564d7ef702ac18c845d7ca42e0"
  },
  {
    "url": "video/mp4_Videos/fixieren_SP.m4v",
    "revision": "cdb8e7b57180d876625e40b05dcd7893"
  },
  {
    "url": "video/mp4_Videos/handstueck_DE.m4v",
    "revision": "713163fcee9807b5aac7a95690902a63"
  },
  {
    "url": "video/mp4_Videos/handstueck_EN.m4v",
    "revision": "d7a40973079c26d195ca043d5278ab4d"
  },
  {
    "url": "video/mp4_Videos/handstueck_FR.m4v",
    "revision": "b718f8caa6a8aceae94271887261d0ee"
  },
  {
    "url": "video/mp4_Videos/handstueck_IT.m4v",
    "revision": "3b963cbd3e269f6ff51055b87ed012fe"
  },
  {
    "url": "video/mp4_Videos/handstueck_PO.m4v",
    "revision": "af0c150735c632385773946641c7f68a"
  },
  {
    "url": "video/mp4_Videos/handstueck_SP.m4v",
    "revision": "3958a069904f9de90455ae9c58458150"
  },
  {
    "url": "video/mp4_Videos/mikroskop_DE.m4v",
    "revision": "f77aadfc984a4e74ef4f42288ad32354"
  },
  {
    "url": "video/mp4_Videos/mikroskop_EN.m4v",
    "revision": "2477991c1663b5916a1fca5f4c7c9ac7"
  },
  {
    "url": "video/mp4_Videos/mikroskop_FR.m4v",
    "revision": "5e1dd768a42f7d2312f9ff666bd2d8ea"
  },
  {
    "url": "video/mp4_Videos/mikroskop_IT.m4v",
    "revision": "9200734410100c726dfd4140f5171da7"
  },
  {
    "url": "video/mp4_Videos/mikroskop_PO.m4v",
    "revision": "e0f627571a71f2a2b6b2d6a692c959fc"
  },
  {
    "url": "video/mp4_Videos/mikroskop_SP.m4v",
    "revision": "23ab63cc6b1d50a6091508ad6b2275ad"
  },
  {
    "url": "video/mp4_Videos/speedfunktion_DE.m4v",
    "revision": "dc5c42e47b5e5f351a9b8525cfea7d1d"
  },
  {
    "url": "video/mp4_Videos/speedfunktion_EN.m4v",
    "revision": "48727ac6b3869139d8e8b23f57cd666e"
  },
  {
    "url": "video/mp4_Videos/speedfunktion_FR.m4v",
    "revision": "f41aa13fb73a41c94ed1d0229a42b4ff"
  },
  {
    "url": "video/mp4_Videos/speedfunktion_IT.m4v",
    "revision": "705cfa5c14358a783b9edb09fc5daa19"
  },
  {
    "url": "video/mp4_Videos/speedfunktion_PO.m4v",
    "revision": "cae6a3d8a4040b21f823379660007bcb"
  },
  {
    "url": "video/mp4_Videos/speedfunktion_SP.m4v",
    "revision": "87f57eac749b0bf8c1929456ffe9cf3a"
  },
  {
    "url": "webfonts/fa-brands-400.eot",
    "revision": "a7b95dbdd87e0c809570affaf366a434"
  },
  {
    "url": "webfonts/fa-brands-400.svg",
    "revision": "f4ac471ba6096eff2a3b1663c87fb067"
  },
  {
    "url": "webfonts/fa-brands-400.ttf",
    "revision": "98b6db59be947f563350d2284fc9ea36"
  },
  {
    "url": "webfonts/fa-brands-400.woff",
    "revision": "2ef8ba3410dcc71578a880e7064acd7a"
  },
  {
    "url": "webfonts/fa-brands-400.woff2",
    "revision": "5e2f92123d241cabecf0b289b9b08d4a"
  },
  {
    "url": "webfonts/fa-regular-400.eot",
    "revision": "dcce4b7fbd5e895561e18af4668265af"
  },
  {
    "url": "webfonts/fa-regular-400.svg",
    "revision": "2d6455887f0541a87c220e34bb11c4ad"
  },
  {
    "url": "webfonts/fa-regular-400.ttf",
    "revision": "65b9977aa23185e8964b36eddbce7a20"
  },
  {
    "url": "webfonts/fa-regular-400.woff",
    "revision": "427d721b86fc9c68b2e85ad42b69238c"
  },
  {
    "url": "webfonts/fa-regular-400.woff2",
    "revision": "e6257a726a0cf6ec8c6fec22821c055f"
  },
  {
    "url": "webfonts/fa-solid-900.eot",
    "revision": "46e7cec623d8bd790d9fdbc8de2d3ee7"
  },
  {
    "url": "webfonts/fa-solid-900.svg",
    "revision": "e1243334186c2183ec1cb2a0893beef3"
  },
  {
    "url": "webfonts/fa-solid-900.ttf",
    "revision": "ff8d9f8adb0d09f11d4816a152672f53"
  },
  {
    "url": "webfonts/fa-solid-900.woff",
    "revision": "a7140145ebaaf5fb14e40430af5d25c4"
  },
  {
    "url": "webfonts/fa-solid-900.woff2",
    "revision": "418dad87601f9c8abd0e5798c0dc1feb"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-background-sync.dev.js",
    "revision": "ed328bc7e31023203c286db32daa2d0c"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-background-sync.prod.js",
    "revision": "39793550463647e7c0de6e596b73b601"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-broadcast-update.dev.js",
    "revision": "d57d0b2581673d91c49ca911a05b0aa2"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-broadcast-update.prod.js",
    "revision": "1a0a5fd8f45cdd7d7622c75c047dd370"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-cacheable-response.dev.js",
    "revision": "1d60c59ad38f262f11ccbdc5e071f203"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-cacheable-response.prod.js",
    "revision": "208ddb754235411cd7ad1505ee219936"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-core.dev.js",
    "revision": "459e08f930061f50c6950c68f4a36816"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-core.prod.js",
    "revision": "6d0787ad5000c8e66c6ba49046cfbd62"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-expiration.dev.js",
    "revision": "83975ed39c895625a8c101b056d91f61"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-expiration.prod.js",
    "revision": "cf90701c83b19c444fdd958a736b3076"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-navigation-preload.dev.js",
    "revision": "277adc85d096aa2027011054172bf8e7"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-navigation-preload.prod.js",
    "revision": "c0b544ce3e822b9f13b7d70e45f420f3"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-offline-ga.dev.js",
    "revision": "d7418bfcbe667500e628de0d64903604"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-offline-ga.prod.js",
    "revision": "1fdae34f7697cd9132f331db17174244"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-precaching.dev.js",
    "revision": "222d289800aa3922b2e197b317c4333f"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-precaching.prod.js",
    "revision": "b036e1a7eacb7d06735c4878173d103c"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-range-requests.dev.js",
    "revision": "3c5a1bf649a929fd5c7618a22b695836"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-range-requests.prod.js",
    "revision": "5c3730ad5204385ea49adb923f8646a2"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-routing.dev.js",
    "revision": "4f1a25c75907dc9190dcdf0a9d3401c0"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-routing.prod.js",
    "revision": "41d9e7f521c3ad95d60921db4346ff20"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-strategies.dev.js",
    "revision": "f24c3e3d147442028af94ea32a7624c3"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-strategies.prod.js",
    "revision": "c11afdb39d8e4e762fac0b36d7e81686"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-streams.dev.js",
    "revision": "6b32f48e4ad3be4487979e0e3d282102"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-streams.prod.js",
    "revision": "a122e0a372bc617d65f7d26f227045a5"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-sw.js",
    "revision": "edf25979d5d95c54e61428c552da0d2e"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-window.dev.es5.mjs",
    "revision": "fc994f936328441cc31ce2c228c273e8"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-window.dev.mjs",
    "revision": "0660ff174ef790c3eb7b804303560220"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-window.dev.umd.js",
    "revision": "a9e46dd4ac9a26189ab41c60dfe296f1"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-window.prod.es5.mjs",
    "revision": "869476238d69f1d95543f9019c9541c6"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-window.prod.mjs",
    "revision": "56cbbcdb311d6e4f35259abf7cc4fb10"
  },
  {
    "url": "workbox/workbox-v4.3.1/workbox-window.prod.umd.js",
    "revision": "2072b10dd5eb2c312c5792ef0ac04336"
  }
]);
