$(function() {      
  $(".content").swipe( {
    swipeLeft:function(event, direction, distance, duration, fingerCount) {
  		window.location.href = $('.pagination a:last').attr('href');
    },
    swipeRight:function(event, direction, distance, duration, fingerCount) {
  		window.location.href = $('.pagination a:first').attr('href');
    },
  });
  $("#content_main").swipe( {
     swipeRight:function(event, direction, distance, duration, fingerCount) {},
  });
});