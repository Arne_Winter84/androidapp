//Service Worker 1.2
var deferredPrompt;
var reg
index = false;

url = window.location.href.split("/");
// console.log(url);
if(url[url.length-1] == 'index.html' || url[url.length-1] == 'index' || url[url.length-1] == ''){
  index = true;
}
// initListener();
if(index){
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('service-worker.js')
      .then(function(reg) {
        console.log('Service worker registered!',reg);
        // window.myWorkerReg = reg;
        reg.installing; // the installing worker, or undefined
        reg.waiting; // the waiting worker, or undefined
        reg.active; // the active worker, or undefined
        reg = reg;
      
      });
  }
}else{
  if ('../serviceWorker' in navigator) {
    navigator.serviceWorker
      .register('service-worker.js')
      .then(function(reg) {
        console.log('Service worker registered else! ',reg);
        // window.myWorkerReg = reg;
        reg.installing; // the installing worker, or undefined
        reg.waiting; // the waiting worker, or undefined
        reg.active; // the active worker, or undefined
        reg = reg;
        
      });
  }
}

// navigator.serviceWorker.addEventListener("message", (evt) => {
//   console.log("hier");
//   window.alert(`Nachricht vom SW: ${ evt.data }`);
// });


$(document).on('show.bs.modal', '#exampleModal', function (e) {
  localStorage.setItem("contentLoaded",1);
});

if ('serviceWorker' in navigator) {
  navigator.serviceWorker.ready
  .then(function(registration) {
    console.log('A service worker is ready:', registration.ready);
    setTimeout(() => {
      let contentLoaded = parseInt( localStorage.getItem("contentLoaded"));
      // console.log("modalHasShown ? ",contentLoaded);
      if(contentLoaded !== 1 )
      {
        $("#exampleModal").modal();
      }

    }, 500);
    
    // At this point, you can call methods that require an active
    // service worker, like registration.pushManager.subscribe()
  });
} else {
  console.log('Service workers are not supported.');
}

// lock Orientation
// var lockFunction =  window.screen.orientation.lock;
// if (lockFunction.call(window.screen.orientation, 'landscape')) {
//   console.log('Orientation locked')
// } else {
//    console.error('There was a problem in locking the orientation')
// }
// EventListener
// function initListener()
// {
  // self.addEventListener('sync', (event) => { 
  //   console.log("synq ",event);
  // }); 
  // self.addEventListener('push', (event) => { 
  //   console.log("push ",event);
  // }); 
  // self.addEventListener('fetch', (event) => { 
  //   console.log("fetch ",event);
  // });


  navigator.serviceWorker.getRegistrations().then(registrations => {
      console.log("registr: ",registrations);
  });
  navigator.serviceWorker.onstatechange = function(statechangeevent) { 
    console.log("s : ",statechangeevent) 
  }
  navigator.serviceWorker.addEventListener('statechange', function(statechangeevent) { 
    console.log("statechangeevent: ",statechangeevent); 
  } )

  // Service Worker-based solution
  self.addEventListener('activate', async () => {
    console.log("activate");
    // after we've taken over, iterate over all the current clients (windows)
    const tabs = await self.clients.matchAll({type: 'window'})
    tabs.forEach((tab) => {
      // ...and refresh each one of them
      console.log("activate2 ",tab);
      tab.navigate(tab.url)
    })
  })



// https://stackoverflow.com/questions/50762626/pwa-beforeinstallprompt-not-called
  window.addEventListener('beforeinstallprompt', function(event) {
    console.log("beforeInstall ",event);
    event.preventDefault();
    deferredPrompt = event;
    // alert("beforeinstall");
    console.log("beforeInstall");
    $("#exampleModal").modal();
    return false;
    
  });


window.addEventListener('appinstalled', (evt) => {
  console.log('a2hs installed');
  // alert("appinstalled");
});
window.addEventListener("load", () => {
  // console.log("load");
  function handleNetworkChange(event) {
    if (navigator.onLine) {
      // alert("navigator.onLine");
      document.body.classList.remove("offline");
    } else {
      // alert("navigator.onLine else");
      document.body.classList.add("offline");
    }
  }
  window.addEventListener("online", handleNetworkChange);
  window.addEventListener("offline", handleNetworkChange);
});


// Ende EventListener

//Pagination

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

//Multilanguage
var lang;
var paginationFirstUsed = false;
var paginationLastUsed = false;
var booted = false;

function lockOrientation()
{
  // https://developers.google.com/web/fundamentals/native-hardware/fullscreen
  // https://code-boxx.com/lock-screen-orientation/
  var isMobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
  console.log(isMobile);
  
  if(isMobile)// && booted === false)
  {
    booted = true;
    console.log("Mobile Usage! - need to lock Orientation = Landscape");
      var element = document.documentElement;
    if(element.requestFullscreen) {
      element.requestFullscreen();
    } else if(element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if(element.msRequestFullscreen) {
      element.msRequestFullscreen();
    } else if(element.webkitRequestFullscreen) {
      element.webkitRequestFullscreen();
    }

    window.screen.orientation.lock("landscape");
  }
  else
  {
    console.log("Desktop!, dont need to lock Orientation = Landscape ",booted);
  }
  $('#lockOrientationModal').modal("hide");
}

$(document).ready(function(){
  if(index){
    $("#lang-select").html('<i class="fas fa-times" id="lang-close" onclick="lang_hide();"></i><center><div class="lang-div"><img src="images/flags/de.png" id="de" onclick="language(this.id);"></img><img src="images/flags/en.png" id="en" onclick="language(this.id);"></img></div><div class="lang-div"><img src="images/flags/po.png" id="po" onclick="language(this.id);"></img><img src="images/flags/it.png" id="it" onclick="language(this.id);"></img></div><div class="lang-div"><img src="images/flags/fr.png" id="fr" onclick="language(this.id);"></img><img src="images/flags/es.png" id="es" onclick="language(this.id);"></img></div><center>');
    $(".head > .header").html('<span style="vertical-align: middle;margin-left: 20px;font-size:30px;cursor:pointer;color: #fff" onclick="openNav()">&#9776;</span><a href="/"><img style="vertical-align: middle" src="images/schwein_transparent_5cm.png" class="logo_main"><h1 style="vertical-align: middle">piggy<sup>®</sup>Teacher</h1></a>');
  }else{
    $("#lang-select").html('<i class="fas fa-times" id="lang-close" onclick="lang_hide();"></i><center><div class="lang-div"><img src="../images/flags/de.png" id="de" onclick="language(this.id);"></img><img src="../images/flags/en.png" id="en" onclick="language(this.id);"></img></div><div class="lang-div"><img src="../images/flags/po.png" id="po" onclick="language(this.id);"></img><img src="../images/flags/it.png" id="it" onclick="language(this.id);"></img></div><div class="lang-div"><img src="../images/flags/fr.png" id="fr" onclick="language(this.id);"></img><img src="../images/flags/es.png" id="es" onclick="language(this.id);"></img></div><center>');
    $(".head > .header").html('<span style="vertical-align: middle;margin-left: 20px;font-size:30px;cursor:pointer;color: #fff" onclick="openNav()">&#9776;</span><a href="/"><img style="vertical-align: middle" src="../images/schwein_transparent_5cm.png" class="logo_main"><h1 style="vertical-align: middle">piggy<sup>®</sup>Teacher</h1></a>');
  }

  lang = document.cookie.replace(/(?:(?:^|.*;\s*)lang\s*\=\s*([^;]*).*$)|^.*$/, "$1");
  if (lang != ''){
    language(lang);
  }else{
    language('de');
  }
  // lockOrientation();
  if(index && booted === false)
  {
    // console.log("boot");
    booted = true;
    // $('#lockOrientationModal').modal();
  }
});

function language(e){
    lang = e;
    document.cookie = "lang=" + e + ";path=/";
    $(".content").html(text[e]);


    if(index){
        $("#mySidenav").html(sidenav_index[e]);
    }else{
        $("#mySidenav").html(sidenav[e]);
    }
    if(paginationFirstUsed){
        $('.pagination a:first').html('&laquo; ' + paginationFirst[e]);
    }
    if(paginationLastUsed){
        $('.pagination a:last').html(paginationLast[e] + ' &raquo;');
    }

    if(index){
      $(".lang-header").html('<img src="images/flags/'+ e + '.png" id="lang-btn" onclick="lang_show(this.id);"></i>');
      setTimeout(() => {
        $("body").append('<script src="js/bs_new/bootstrap.min.js"></script>');

      }, 1000);
    }else {
      $(".lang-header").html('<img src="../images/flags/'+ e + '.png" id="lang-btn" onclick="lang_show(this.id);"></i>');
      $("body").append('<script src="../js/bs_new/bootstrap.min.js"></script>');
    }
    lang_hide();
}

function lang_show(){
    $("#lang-select").show();
}

function lang_hide(){
    $("#lang-select").hide();
}

var sidenav = {
    de: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="/index"><i class="fas fa-home"></i> Home</a><a href="2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Sicherheit</a><a href="8_anschluesse"><i class="fas fa-plug"></i> Anschlüsse</a><a href="11_handstueck"><i class="fas fa-hand-paper"></i> Handstück</a><a href="19_mikroskop"><i class="fas fa-microscope"></i> Mikroskop</a><a href="24_display"><i class="fas fa-tv"></i> Display</a><a href="34_prinzip"><i class="fas fa-list-ul"></i> Prinzip</a><a href="51_draehte"><i class="fas fa-grip-lines-vertical"></i> Drähte</a><span class="tel"><i class="fas fa-phone"></i> +49 2204 - 30 66 0</span>',

    en: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="/index"><i class="fas fa-home"></i> Home</a><a href="2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Safety</a><a href="8_anschluesse"><i class="fas fa-plug"></i> Connections</a><a href="11_handstueck"><i class="fas fa-hand-paper"></i> Handpiece</a><a href="19_mikroskop"><i class="fas fa-microscope"></i> Microscope</a><a href="24_display"><i class="fas fa-tv"></i> Display</a><a href="34_prinzip"><i class="fas fa-list-ul"></i> Principle</a><a href="51_draehte"><i class="fas fa-grip-lines-vertical"></i> Wires</a><span class="tel"><i class="fas fa-phone"></i> +49 2204 - 30 66 0</span>',

    fr: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="/index"><i class="fas fa-home"></i> Home</a><a href="2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Sécurité</a><a href="8_anschluesse"><i class="fas fa-plug"></i> Raccordements</a><a href="11_handstueck"><i class="fas fa-hand-paper"></i> Pièce à main</a><a href="19_mikroskop"><i class="fas fa-microscope"></i> Microscope</a><a href="24_display"><i class="fas fa-tv"></i> Affichage</a><a href="34_prinzip"><i class="fas fa-list-ul"></i> Principe</a><a href="51_draehte"><i class="fas fa-grip-lines-vertical"></i> Fils</a><span class="tel"><i class="fas fa-phone"></i> +49 2204 - 30 66 0</span>',

    es: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="/index"><i class="fas fa-home"></i> Home</a><a href="2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Seguridad</a><a href="8_anschluesse"><i class="fas fa-plug"></i> Conexiones</a><a href="11_handstueck"><i class="fas fa-hand-paper"></i> Manípulo</a><a href="19_mikroskop"><i class="fas fa-microscope"></i> Microscopio</a><a href="24_display"><i class="fas fa-tv"></i> Display</a><a href="34_prinzip"><i class="fas fa-list-ul"></i> Principio</a><a href="51_draehte"><i class="fas fa-grip-lines-vertical"></i> Alambres</a><span class="tel"><i class="fas fa-phone"></i> +49 2204 - 30 66 0</span>',

    po: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="/index"><i class="fas fa-home"></i> Home</a><a href="2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Segurança</a><a href="8_anschluesse"><i class="fas fa-plug"></i> Conexões</a><a href="11_handstueck"><i class="fas fa-hand-paper"></i> Peça de mão</a><a href="19_mikroskop"><i class="fas fa-microscope"></i> Microscópio</a><a href="24_display"><i class="fas fa-tv"></i> Monitor</a><a href="34_prinzip"><i class="fas fa-list-ul"></i> Conceitos básicos</a><a href="51_draehte"><i class="fas fa-grip-lines-vertical"></i> Fios</a><span class="tel"><i class="fas fa-phone"></i> +49 2204 - 30 66 0</span>',

    it: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="/index"><i class="fas fa-home"></i> Home</a><a href="2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Sicurezza</a><a href="8_anschluesse"><i class="fas fa-plug"></i> Connessioni</a><a href="11_handstueck"><i class="fas fa-hand-paper"></i> Manipolo</a><a href="19_mikroskop"><i class="fas fa-microscope"></i> Microscopio</a><a href="24_display"><i class="fas fa-tv"></i> Display</a><a href="34_prinzip"><i class="fas fa-list-ul"></i> Principio</a><a href="51_draehte"><i class="fas fa-grip-lines-vertical"></i> Fili</a><span class="tel"><i class="fas fa-phone"></i> +49 2204 - 30 66 0</span>',
}

var sidenav_index = {
    de: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="#"><i class="fas fa-home"></i> Home</a><a href="seiten/2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="seiten/3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Sicherheit</a><a href="seiten/8_anschluesse"><i class="fas fa-plug"></i> Anschlüsse</a><a href="seiten/11_handstueck"><i class="fas fa-hand-paper"></i> Handstück</a><a href="seiten/19_mikroskop"><i class="fas fa-microscope"></i> Mikroskop</a><a href="seiten/24_display"><i class="fas fa-tv"></i> Display</a><a href="seiten/34_prinzip"><i class="fas fa-list-ul"></i> Prinzip</a><a href="seiten/51_draehte"><i class="fas fa-grip-lines-vertical"></i> Drähte</a><span class="tel"><i class="fas fa-phone"></i> 02204 - 30 66 0</span>',

    en: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="#"><i class="fas fa-home"></i> Home</a><a href="seiten/2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="seiten/3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Safety</a><a href="seiten/8_anschluesse"><i class="fas fa-plug"></i> Connections</a><a href="seiten/11_handstueck"><i class="fas fa-hand-paper"></i> Handpiece</a><a href="seiten/19_mikroskop"><i class="fas fa-microscope"></i> Microscope</a><a href="seiten/24_display"><i class="fas fa-tv"></i> Display</a><a href="seiten/34_prinzip"><i class="fas fa-list-ul"></i> Principle</a><a href="seiten/51_draehte"><i class="fas fa-grip-lines-vertical"></i> Wires</a><span class="tel"><i class="fas fa-phone"></i> 02204 - 30 66 0</span>',

    fr: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="#"><i class="fas fa-home"></i> Home</a><a href="seiten/2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="seiten/3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Sécurité</a><a href="seiten/8_anschluesse"><i class="fas fa-plug"></i> Raccordements</a><a href="seiten/11_handstueck"><i class="fas fa-hand-paper"></i> Pièce à main</a><a href="seiten/19_mikroskop"><i class="fas fa-microscope"></i> Microscope</a><a href="seiten/24_display"><i class="fas fa-tv"></i> Affichage</a><a href="seiten/34_prinzip"><i class="fas fa-list-ul"></i> Principe</a><a href="seiten/51_draehte"><i class="fas fa-grip-lines-vertical"></i> Fils</a><span class="tel"><i class="fas fa-phone"></i> 02204 - 30 66 0</span>',

    es: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="#"><i class="fas fa-home"></i> Home</a><a href="seiten/2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="seiten/3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Seguridad</a><a href="seiten/8_anschluesse"><i class="fas fa-plug"></i> Conexiones</a><a href="seiten/11_handstueck"><i class="fas fa-hand-paper"></i> Manípulo</a><a href="seiten/19_mikroskop"><i class="fas fa-microscope"></i> Microscopio</a><a href="seiten/24_display"><i class="fas fa-tv"></i> Display</a><a href="seiten/34_prinzip"><i class="fas fa-list-ul"></i> Principio</a><a href="seiten/51_draehte"><i class="fas fa-grip-lines-vertical"></i> Alambres</a><span class="tel"><i class="fas fa-phone"></i> 02204 - 30 66 0</span>',

    po: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="#"><i class="fas fa-home"></i> Home</a><a href="seiten/2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="seiten/3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Segurança</a><a href="seiten/8_anschluesse"><i class="fas fa-plug"></i> Conexões</a><a href="seiten/11_handstueck"><i class="fas fa-hand-paper"></i> Peça de mão</a><a href="seiten/19_mikroskop"><i class="fas fa-microscope"></i> Microscópio</a><a href="seiten/24_display"><i class="fas fa-tv"></i> Monitor</a><a href="seiten/34_prinzip"><i class="fas fa-list-ul"></i> Conceitos básicos</a><a href="seiten/51_draehte"><i class="fas fa-grip-lines-vertical"></i> Fios</a><span class="tel"><i class="fas fa-phone"></i> 02204 - 30 66 0</span>',

    it: '<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><a href="#"><i class="fas fa-home"></i> Home</a><a href="seiten/2_piggyWelder"><i class="fas fa-hammer"></i> piggyWelder®</a><a href="seiten/3_sicherheitsvorschriften"><i class="fas fa-hard-hat"></i> Sicurezza</a><a href="seiten/8_anschluesse"><i class="fas fa-plug"></i> Connessioni</a><a href="seiten/11_handstueck"><i class="fas fa-hand-paper"></i> Manipolo</a><a href="seiten/19_mikroskop"><i class="fas fa-microscope"></i> Microscopio</a><a href="seiten/24_display"><i class="fas fa-tv"></i> Display</a><a href="seiten/34_prinzip"><i class="fas fa-list-ul"></i> Principio</a><a href="seiten/51_draehte"><i class="fas fa-grip-lines-vertical"></i> Fili</a><span class="tel"><i class="fas fa-phone"></i> 02204 - 30 66 0</span>',
}

function progressbarStart()
{

  progressbar.interval = setInterval(function(){ 
    progressbar.percentProgress += ((progressbar.intervalTime / progressbar.endTime)*100);
    progressbar.value += 1; 
    $('.progress-bar').first().css("width", progressbar.percentProgress + "%");
    $('.progress-bar').first().html( progressbar.percentProgress.toFixed(2) + "%");
    // console.log(progressbar.percentProgress);
    if(progressbar.percentProgress >= progressbar.maxValue)
    {
      clearInterval(progressbar.interval);
      $('.progress-bar').first().html( "100%");
      $('.progress-bar').first().removeClass('progress-bar-animated');
    }
  }, progressbar.intervalTime);
}

